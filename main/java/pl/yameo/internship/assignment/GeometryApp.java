package pl.yameo.internship.assignment;

import java.util.List;
import java.util.Scanner;

public class GeometryApp {
	private Scanner scanner;
	private ShapeManager shapeManager;

	public GeometryApp(Scanner scanner) {
		this.scanner = scanner;
		shapeManager = new ShapeManager(scanner);
	}

	public void start() {
		boolean run = true;
		while (run) {
			run = run();
		}
	}

	private boolean run() {
		Messenger.showMenuOptions();

		int option = readInteger();
		switch (option){
			case 0:
				return false;
			case 1:
				createNewShape();
				break;
			case 2:
				listShapes();
				break;
			case 3:
				modifyShape();
				break;
		}

		return true;
	}

	private void createNewShape(){
		Messenger.showShapesOptions();

		int option = readInteger();
		shapeManager.createNewShape(option);
	}

	private void listShapes() {
		Messenger.showStartLisitngMessage();
		shapeManager.listShapes();
		Messenger.showEndLisitngMessage();
	}

	private void modifyShape() {
		listShapes();
		Messenger.showChooseShapeIndexToModifyMessage(shapeManager.getShapesCount());

		Shape activeShape = shapeManager.getShape(readInteger());
		if(activeShape == null){
			Messenger.showIncorrectIndexMessage();
			return;
		}

		List<Double> oldDimensions = activeShape.listDimensions();
		Double oldPerimeter = activeShape.calculatePerimeter();
		Double oldArea = activeShape.calculateArea();

		Messenger.showShapeFeatures(activeShape, oldDimensions, oldArea, oldPerimeter);
		shapeManager.modifyShape(activeShape);
		Messenger.showOldAndNewComparison(activeShape, oldDimensions, oldArea, oldPerimeter);
	}

	private Integer readInteger() {
		Integer value = null;
		while (value == null) {
			if (scanner.hasNextInt()) {
				value = scanner.nextInt();
			} else {
				scanner.next();
			}
		}

		return value;
	}
}
