package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Ellipse implements Shape {
	private static final int PARAMETERS_COUNT = 2;
	private Double semiMajorAxis;
	private Double semiMinorAxis;

	public Ellipse(){
		semiMajorAxis = 0.0;
		semiMinorAxis = 0.0;
	}

	public Ellipse(Double semiMajorAxis, Double semiMinorAxis) {
		this.semiMajorAxis = semiMajorAxis;
		this.semiMinorAxis = semiMinorAxis;
	}

	@Override
	public String getName() {
		return "Ellipse";
	}

	@Override
	public final List<Double> listDimensions() {
		return Arrays.asList(semiMajorAxis, semiMinorAxis);
	}

	@Override
	public final Double calculateArea() {
		return Math.PI * (3 * (semiMajorAxis + semiMinorAxis) / 2 - Math.sqrt(semiMajorAxis * semiMinorAxis));
	}

	@Override
	public final Double calculatePerimeter() {
		return Math.PI * semiMajorAxis * semiMinorAxis;
	}

	@Override
	public int getParametersCount() {
		return PARAMETERS_COUNT;
	}

	@Override
	public void setParameters(List<Double> parameters) {
		setSemiMajorAxis(parameters.get(0));
		setSemiMinorAxis(parameters.get(1));
	}

	@Override
	public String getReadMessage(){
		return "Please provide two semi-axis lengths (major, minor):";
	}

	public void setSemiMajorAxis(Double semiMajorAxis) {
		this.semiMajorAxis = semiMajorAxis;
	}

	public void setSemiMinorAxis(Double semiMinorAxis) {
		this.semiMinorAxis = semiMinorAxis;
	}
}
