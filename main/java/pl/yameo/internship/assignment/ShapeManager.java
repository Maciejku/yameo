package pl.yameo.internship.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ShapeManager {
    private List<Shape> shapes;
    private Scanner scanner;

    public ShapeManager(Scanner scanner){
        shapes = new ArrayList<>();
        this.scanner = scanner;
    }

    public void listShapes() {
        shapes.forEach(shape -> {
            System.out.print(shape.getName() + " with dimensions: ");
            System.out.print(shape.listDimensions() + "; ");
            System.out.print("Area: " + shape.calculateArea() + "; ");
            System.out.println("Perimeter: " + shape.calculatePerimeter());
        });
    }

    public Shape getShape(int index){
        Shape activeShape = null;

        if(index > 0 && index <= shapes.size()) //TODO exception ?
            activeShape = shapes.get(index - 1);

        return activeShape;
    }

    public void createNewShape(int chosenShape){
        Shape newShape = createShape(chosenShape);
        if (newShape != null) {
            addShape(newShape);
        }
    }

    public int getShapesCount(){
        return shapes.size();
    }

    public void modifyShape(Shape activeShape){
        List<Double> parameters = new ArrayList<>();

        System.out.println(activeShape.getReadMessage());

        int parametersToRead = activeShape.getParametersCount();
        while (parametersToRead > 0){
            parameters.add(readDouble());
            parametersToRead--;
        }

        activeShape.setParameters(parameters);
    }

    private void addShape(Shape newShape){
        shapes.add(newShape);
    }

    private Shape createShape(int chosenShape) {
        switch (chosenShape){
            case 1:
                return createNewEllipse();
            case 2:
                return createNewRectangle();
            case 3:
                return createNewCircle();
            case 4:
                return createNewSquare();
            case 5:
                return createNewTriangle();
            case 6:
                return createNewHexagon();
            default:
                return null;
        }
    }

    private Hexagon createNewHexagon(){
        System.out.println("Please provide edge length:");
        return new Hexagon(readDouble());
    }

    private Ellipse createNewEllipse() {
        System.out.println("Please provide two semi-axis lengths (major, minor):");
        return new Ellipse(readDouble(), readDouble());
    }

    private Rectangle createNewRectangle() {
        System.out.println("Please provide two edge lengths (height, width):");
        return new Rectangle(readDouble(), readDouble());
    }

    private Circle createNewCircle() {
        System.out.println("Please provide the radius for the circle:");
        return new Circle(readDouble());
    }

    private Square createNewSquare() {
        System.out.println("Please provide the edge length:");
        return new Square(readDouble());
    }

    private Triangle createNewTriangle() {
        System.out.println("Please provide three edge lengths:");
        return new Triangle(readDouble(), readDouble(), readDouble());
    }

    private Double readDouble() {
        Double value = null;
        while (value == null) {
            if (scanner.hasNextDouble()) {
                value = scanner.nextDouble();
                if(value <= 0) {
                    value = null;
                    System.out.println("Value must be greater than 0 !");
                }
            } else {
                scanner.next();
            }
        }

        return value;
    }
}
