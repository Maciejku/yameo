package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Hexagon implements Shape {
    private static final int PARAMETERS_COUNT = 1;
    private Double edge;

    public Hexagon(){
        edge = 0.0;
    }

    public Hexagon(Double edge){
        this.edge = edge;
    }

    @Override
    public String getName() {
        return "Hexagon";
    }

    @Override
    public List<Double> listDimensions() {
        return  Arrays.asList(edge);
    }

    @Override
    public Double calculateArea() {
        return edge * edge * Math.sqrt(6.75d);
    }

    @Override
    public Double calculatePerimeter() {
        return 6 * edge;
    }

    @Override
    public int getParametersCount() {
        return PARAMETERS_COUNT;
    }

    @Override
    public String getReadMessage() {
        return "Please provide one edge length:";
    }

    @Override
    public void setParameters(List<Double> parameters) {
        setEdge(parameters.get(0));
    }

    public void setEdge(Double edge) {
        this.edge = edge;
    }
}
