package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Triangle implements Shape {
	private static final int PARAMETERS_COUNT = 3;
	private Double edgeA;
	private Double edgeB;
	private Double edgeC;

	public Triangle(){
		edgeA = 0.0;
		edgeB = 0.0;
		edgeC = 0.0;
	}

	public Triangle(Double edgeA, Double edgeB, Double edgeC) {
		this.edgeA = edgeA;
		this.edgeB = edgeB;
		this.edgeC = edgeC;
	}

	@Override
	public String getName() {
		return "Triangle";
	}

	@Override
	public final List<Double> listDimensions() {
		return Arrays.asList(edgeA, edgeB, edgeC);
	}

	@Override
	public final Double calculateArea() {
		Double halfPerimeter = calculatePerimeter() / 2;
		return Math.sqrt(halfPerimeter * (halfPerimeter - edgeA) * (halfPerimeter - edgeB) * (halfPerimeter - edgeC));
	}

	@Override
	public final Double calculatePerimeter() {
		return edgeA + edgeB + edgeC;
	}

	@Override
	public int getParametersCount() {
		return PARAMETERS_COUNT;
	}

	@Override
	public void setParameters(List<Double> parameters) {
		setEdgeA(parameters.get(0));
		setEdgeB(parameters.get(1));
		setEdgeC(parameters.get(2));
	}

	@Override
	public String getReadMessage(){
		return "Please provide three edge lengths:";
	}

	public void setEdgeA(Double edgeA) {
		this.edgeA = edgeA;
	}

	public void setEdgeB(Double edgeB) {
		this.edgeB = edgeB;
	}

	public void setEdgeC(Double edgeC) {
		this.edgeC = edgeC;
	}
}
