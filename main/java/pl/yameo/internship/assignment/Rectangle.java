package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Rectangle implements Shape {
	private static final int PARAMETERS_COUNT = 2;
	private Double height;
	private Double width;

	public Rectangle(){
		height = 0.0;
		width = 0.0;
	}

	public Rectangle(Double height, Double width) {
		this.height = height;
		this.width = width;
	}

	@Override
	public String getName() {
		return "Rectangle";
	}

	@Override
	public final List<Double> listDimensions() {
		return Arrays.asList(height, width);
	}

	@Override
	public final Double calculateArea() {
		return height * width;
	}

	@Override
	public final Double calculatePerimeter() {
		return 2 * (height + width);
	}

	@Override
	public int getParametersCount() {
		return PARAMETERS_COUNT;
	}

	@Override
	public void setParameters(List<Double> parameters) {
		setHeight(parameters.get(0));
		setWidth(parameters.get(1));
	}

	@Override
	public String getReadMessage(){
		return "Please provide two edge lengths (height, width):";
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public void setWidth(Double width) {
		this.width = width;
	}
}
