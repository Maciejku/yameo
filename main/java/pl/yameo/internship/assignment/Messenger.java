package pl.yameo.internship.assignment;

import java.util.List;

public final class Messenger {
    private Messenger(){
    }

    public static void showMenuOptions(){
        System.out.println("Choose action:");
        System.out.println("1) Create new shape");
        System.out.println("2) List existing shapes");
        System.out.println("3) Modify one of the shapes from the list");
        System.out.println("0) Exit");
    }

    public static void showShapesOptions(){
        System.out.println("Choose shape to create:");
        System.out.println("1) Ellipse");
        System.out.println("2) Rectangle");
        System.out.println("3) Circle");
        System.out.println("4) Square");
        System.out.println("5) Triangle");
        System.out.println("6) Hexagon");
        System.out.println("0) Back");
    }

    public static void showShapeFeatures(Shape activeShape, List<Double> oldDimensions, Double oldArea, Double oldPerimeter){
        System.out.print(activeShape.getName() + " with dimensions: ");
        System.out.print(oldDimensions + "; ");
        System.out.print("Area: " + oldArea + "; ");
        System.out.println("Perimeter: " + oldPerimeter);
    }

    public static void showOldAndNewComparison(Shape activeShape, List<Double> oldDimensions, Double oldArea, Double oldPerimeter){
        System.out.println("Old shape: ");
        System.out.print(activeShape.getName() + " with dimensions: ");
        System.out.print(oldDimensions + "; ");
        System.out.print("Area: " + oldArea + "; ");
        System.out.println("Perimeter: " + oldPerimeter);
        System.out.println("============================");

        System.out.println("New shape: ");
        System.out.print(activeShape.getName() + " with dimensions: ");
        System.out.print(activeShape.listDimensions() + "; ");
        System.out.print("Area: " + activeShape.calculateArea() + "; ");
        System.out.println("Perimeter: " + activeShape.calculatePerimeter());
        System.out.println("============================");
    }

    public static void showStartLisitngMessage(){
        System.out.println("====== LIST OF SHAPES ======");
    }

    public static void showEndLisitngMessage(){
        System.out.println("============================");
    }

    public static void showChooseShapeIndexToModifyMessage(int shapesCount){
        System.out.println("Please choose the index of the shape you want to modify (1-" + shapesCount + "): ");
    }

    public static void showIncorrectIndexMessage(){
        System.out.println("Please provide correct index !");
    }


}
